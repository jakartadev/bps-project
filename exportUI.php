<?php
$connect = mysqli_connect("localhost", "root", "", "BPSSIMPEG");
$sql = "SELECT * FROM tbpegawai";  
$result = mysqli_query($connect, $sql);
?>
<html>  
 <head>  
  <title>Export MySQL Data Simpeg ke Excel</title>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
 </head>  
 <body>  
  <div class="container">  
   <br />  
   <br />  
   <br />  
   <div class="table-responsive">  
    <h2 align="center">Export MySQL data ke Excel</h2><br /> 
    <table class="table table-bordered">
     <tr>  
                         <th>NIP</th>  
                         <th>Nama</th>  
                         <th>Kota</th>  
						 <th>Kode Dept</th>
                    </tr>
     <?php
     while($row = mysqli_fetch_array($result))  
     {  
        echo '  
       <tr>  
         <td>'.$row["NIP"].'</td>  
         <td>'.$row["NAMA"].'</td>  
         <td>'.$row["KOTA"].'</td>  
         <td>'.$row["KODEDEPT"].'</td>  
       </tr>  
        ';  
     }
     ?>
    </table>
    <br />
    <form method="post" action="admin\export.php">
     <input type="submit" name="export" class="btn btn-success" value="Export" />
    </form>
   </div>  
  </div>  
 </body>  
</html>